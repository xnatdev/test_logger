package org.nrg.xnat.test_logger;

import java.io.File;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class XnatUtils {

    public static final String XNAT_HOME = System.getProperty("xnat.home");
    public static final File XNAT_LOGS = Paths.get(XNAT_HOME, "logs").toFile();
    public static final File TOMCAT_LOGS = new File("/var/log/tomcat7"); // hard-coded for now

}
