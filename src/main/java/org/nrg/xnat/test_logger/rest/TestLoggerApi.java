package org.nrg.xnat.test_logger.rest;

import io.swagger.annotations.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnat.services.XnatAppInfo;
import org.nrg.xnat.test_logger.git.GitRepo;
import org.nrg.xnat.test_logger.git.NoSuchCommitException;
import org.nrg.xnat.test_logger.git.TomcatLogRepo;
import org.nrg.xnat.test_logger.git.XnatLogRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Api(description = "XNAT Test Logger API")
@XapiRestController
@RequestMapping(value = "/testlog")
public class TestLoggerApi extends AbstractXapiRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestLoggerApi.class);
    private final XnatAppInfo xnatAppInfo;
    private GitRepo xnatLogs, tomcatLogs;

    @Autowired
    public TestLoggerApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final XnatAppInfo appInfo) {
        super(userManagementService, roleHolder);
        xnatAppInfo = appInfo;

    }

    @ApiOperation(value = "Inits git repos for test logging.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Init successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "init", method = RequestMethod.POST)
    public ResponseEntity<String> init() {
        final HttpStatus status = isPermitted();
        if (status != null) {
            return new ResponseEntity<>(status);
        }

        try {
            xnatLogs = new XnatLogRepo();
            xnatLogs.init();
            tomcatLogs = new TomcatLogRepo();
            tomcatLogs.init();
            return new ResponseEntity<>("Repos created.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error in creating repos:\n" + e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Commits changed log files with test name as commit message.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Commit successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "commit/{test}", method = RequestMethod.POST)
    public ResponseEntity<String> commitTest(@ApiParam("The name of the test that has just completed.") @PathVariable final String test) {
        final HttpStatus status = isPermitted();
        if (status != null) {
            return new ResponseEntity<>(status);
        }

        try {
            refreshGit();

            xnatLogs.commitTest(test);
            tomcatLogs.commitTest(test);

            return new ResponseEntity<>("Changes committed.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error in creating repos:\n" + e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Returns a zip of log files which updated during a test.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 404, message = "Requested test commit was not found."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "log/{test}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> getTestLog(@ApiParam("The name of the test that has just completed.") @PathVariable final String test) {
        // See: http://www.leveluplunch.com/java/tutorials/032-return-file-from-spring-rest-webservice/
        final String folderName = "xnatLogs";
        final HttpStatus status = isPermitted();
        if (status != null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        try {
            final Path temp = Files.createTempDirectory("testlogger");
            final Path zip = temp.getParent().resolve(folderName + ".zip");
            refreshGit();
            xnatLogs.getCommit(test, temp);
            tomcatLogs.getCommit(test, temp);
            if (zip.toFile().exists()) {
                Files.delete(zip);
            }
            pack(temp, zip);

            return zipResponse(zip, String.format("%s_%s_logs.zip", test, xnatAppInfo.getCommit()));
        } catch (NoSuchCommitException nsce) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Returns a zip of XNAT and tomcat logs.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Get successful."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Only site admins may access this resource."),
            @ApiResponse(code = 500, message = "Unexpected error.")
    })
    @XapiRequestMapping(value = "gitAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<InputStreamResource> gitAll() {
        // See: http://www.leveluplunch.com/java/tutorials/032-return-file-from-spring-rest-webservice/
        final String folderName = "xnatLogs";
        final HttpStatus status = isPermitted();
        if (status != null) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        try {
            final Path temp = Files.createTempDirectory("testlogger");
            final Path zip = temp.getParent().resolve(folderName + ".zip");
            refreshGit();
            xnatLogs.gitAll(temp);
            tomcatLogs.gitAll(temp);
            if (zip.toFile().exists()) {
                Files.delete(zip);
            }
            pack(temp, zip);

            return zipResponse(zip, String.format("xnat_logs_%s.zip", xnatAppInfo.getCommit()));
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void refreshGit() throws GitAPIException, IOException {
        if (xnatLogs == null || tomcatLogs == null) {
            xnatLogs = new XnatLogRepo();
            xnatLogs.findRepo();
            tomcatLogs = new TomcatLogRepo();
            tomcatLogs.findRepo();
        }
    }

    private void pack(final Path folder, final Path zipFilePath) throws IOException {
        // from http://stackoverflow.com/a/35158142/5128397
        try (
                FileOutputStream fos = new FileOutputStream(zipFilePath.toFile());
                ZipOutputStream zos = new ZipOutputStream(fos)
        ) {
            Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    zos.putNextEntry(new ZipEntry(folder.relativize(file).toString()));
                    Files.copy(file, zos);
                    zos.closeEntry();
                    return FileVisitResult.CONTINUE;
                }

                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    zos.putNextEntry(new ZipEntry(folder.relativize(dir).toString() + "/"));
                    zos.closeEntry();
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    private ResponseEntity<InputStreamResource> zipResponse(Path zippedFile, String downloadedName) throws FileNotFoundException {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentLength(zippedFile.toFile().length());
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.set("content-disposition", "attachment; filename=" + downloadedName);
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(new FileInputStream(zippedFile.toFile())));
    }

}
