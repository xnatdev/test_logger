package org.nrg.xnat.test_logger.git;

import org.eclipse.jgit.api.errors.GitAPIException;

public class NoSuchCommitException extends GitAPIException {

    public NoSuchCommitException(String message) {
        super(message);
    }

}
