package org.nrg.xnat.test_logger.git;

import org.nrg.xnat.test_logger.XnatUtils;

public class XnatLogRepo extends GitRepo {

    public XnatLogRepo() {
        super(XnatUtils.XNAT_LOGS);
    }

    public String getSubdirName() {
        return "xnat";
    }

}
