package org.nrg.xnat.test_logger.git;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.Edit;
import org.eclipse.jgit.patch.FileHeader;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.io.NullOutputStream;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class GitRepo {

    private File location;
    private Git git;

    public GitRepo(File location) {
        this.location = location;
    }

    public void init() throws IOException, GitAPIException {
        destroy();
        create();
    }

    public void destroy() throws IOException {
        FileUtils.deleteDirectory(Paths.get(location.getAbsolutePath(), ".git").toFile());
    }

    public void create() throws GitAPIException {
        git = Git.init().setDirectory(location).call();
        commit("Initial state");
    }

    public void commit(String message) throws GitAPIException {
        git.add().addFilepattern(".").call();
        git.commit().setMessage(message).call();
    }

    public void findRepo() throws GitAPIException, IOException {
        git = Git.open(location);
    }

    public void commitTest(String test) throws GitAPIException {
        commit(test);
    }

    public void getCommit(String test, Path tempFolder) throws GitAPIException, IOException {
        final Path tempSubfolder = tempFolder.resolve(getSubdirName());

        final Iterator<RevCommit> iterator = git.log().call().iterator();
        RevCommit testCommit = null;
        while (iterator.hasNext()) {
            final RevCommit commit = iterator.next();
            if (commit.getShortMessage().equals(test)) {
                testCommit = commit;
                break;
            }
        }

        if (testCommit == null) {
            throw new NoSuchCommitException("Could not find commit: " + test);
        }

        final List<DiffEntry> diffs;

        try (DiffFormatter diffFormatter = new DiffFormatter(NullOutputStream.INSTANCE)) {
            diffFormatter.setRepository(git.getRepository());
            diffs = diffFormatter.scan(testCommit.getParent(0), testCommit);
            for (final DiffEntry diff : diffs) {
                final FileHeader fileHeader = diffFormatter.toFileHeader(diff);
                final String file = fileHeader.getNewPath();
                final Edit edit = fileHeader.toEditList().get(0);
                final int start = edit.getBeginB() + 1;
                final int end = edit.getEndB();
                final Path fileDiff = tempSubfolder.resolve(file);
                Files.createDirectories(fileDiff.getParent());

                final FileInputStream fileInputStream = new FileInputStream(new File(location, file));
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                final List<String> diffLines = new ArrayList<>();
                for (int i = 0; i < start - 1; i++) {
                    bufferedReader.readLine();
                }
                for (int i = 0; i < end - start + 1; i++) {
                    diffLines.add(bufferedReader.readLine());
                }
                bufferedReader.close();
                fileInputStream.close();
                Files.write(fileDiff, diffLines, Charset.defaultCharset());
            }
        }

    }

    public void gitAll(Path tempFolder) throws GitAPIException, IOException {
        final Path tempSubfolder = tempFolder.resolve(getSubdirName());
        FileUtils.copyDirectory(location, tempSubfolder.toFile());
    }

    public abstract String getSubdirName();

}
