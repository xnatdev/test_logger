package org.nrg.xnat.test_logger.git;

import org.nrg.xnat.test_logger.XnatUtils;

public class TomcatLogRepo extends GitRepo {

    public TomcatLogRepo() {
        super(XnatUtils.TOMCAT_LOGS);
    }

    public String getSubdirName() {
        return "tomcat";
    }

}
