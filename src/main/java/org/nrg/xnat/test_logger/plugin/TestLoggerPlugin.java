package org.nrg.xnat.test_logger.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "testLoggerPlugin", name = "XNAT Test Logging Plugin",
        description = "This plugin turns the XNAT and tomcat logs into git repos divided up by automated test which ran against the server.")
@ComponentScan({"org.nrg.xnat.test_logger.rest"})
public class TestLoggerPlugin {
}
